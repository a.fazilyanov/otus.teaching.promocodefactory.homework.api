﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Grpc

{
    public class CustomerService : GrpcCustomersService.GrpcCustomersServiceBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<GrpcCustomersResponse> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();
            var customersResponse = new GrpcCustomersResponse();
            customersResponse.Customers.AddRange(customers.Select(CustomerMapper.ToModelGrpcCustomerResponse));
            return await Task.FromResult(customersResponse);
        }


        public override async Task<GrpcCustomerResponse> GetCustomer(StringValue id, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(id.Value));
            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));

            return await Task.FromResult(CustomerMapper.ToModelGrpcCustomerResponse(customer));
        }

        public override async Task<GrpcCustomerResponse> CreateCustomer(GrpcCreateOrEditCustomerRequest request,
            ServerCallContext context)
        {
            var preferences =
                await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());
            var customer = CustomerMapper.MapFromModel(request, preferences);
            await _customerRepository.AddAsync(customer);

            return await Task.FromResult(CustomerMapper.ToModelGrpcCustomerResponse(customer));
        }

        public override async Task<GrpcCustomerResponse> EditCustomer(GrpcCreateOrEditCustomerRequest request,
            ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));

            var preferences =
                await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());

            CustomerMapper.MapFromModel(request, preferences, customer);
            await _customerRepository.UpdateAsync(customer);

            return await Task.FromResult(CustomerMapper.ToModelGrpcCustomerResponse(customer));
        }

        public override async Task<BoolValue> DeleteCustomer(StringValue id, ServerCallContext context)
        {
            var t = new BoolValue();
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(id.Value));
            if (customer == null)
                return await Task.FromResult(t);
            ;

            t.Value = true;
            await _customerRepository.DeleteAsync(customer);
            return await Task.FromResult(t);
        }
    }
}