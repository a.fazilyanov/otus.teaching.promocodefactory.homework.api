﻿﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 using Otus.Teaching.PromoCodeFactory.WebHost.Grpc;

 namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class CustomerMapper
    {

        public static GrpcCustomerResponse ToModelGrpcCustomerResponse(Customer customer)
        {
            if (customer == null) return null;

            var result = new GrpcCustomerResponse()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
            };

            result.Preferences.AddRange(
                customer.Preferences.Select(x =>
                    new GrpcPreferenceResponse
                    {
                        Id = x.PreferenceId.ToString(),
                        Name = x.Preference.Name,
                    }
                ).ToList()
            );

            return result;
        }

        public static Customer MapFromModel(GrpcCreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            customer ??= new Customer {Id = Guid.NewGuid()};

            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }


        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }
            
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            
            return customer;
        }
    }
}
