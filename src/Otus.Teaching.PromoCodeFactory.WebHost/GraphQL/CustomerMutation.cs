﻿using System;
using System.Threading.Tasks;
using HotChocolate;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class CustomerMutation
    {
        public async Task<Customer> CreateCustomerAsync(CreateOrEditCustomerRequest request,
            [Service] IRepository<Customer> customerRepository, [Service] IRepository<Preference> preferenceRepository)
        {
            if (request.PreferenceIds == null)
                return null;

            var preferences = await preferenceRepository.GetRangeByIdsAsync(
                request.PreferenceIds);

            var customer = CustomerMapper.MapFromModel(request, preferences);
            await customerRepository.AddAsync(customer);

            return customer;
        }

        public async Task<Customer> EditCustomerAsync(Guid id, CreateOrEditCustomerRequest request,
            [Service] IRepository<Customer> customerRepository, [Service] IRepository<Preference> preferenceRepository)
        {
            var customer = await customerRepository.GetByIdAsync(id);
            if (customer == null)
                return null;

            if (request.PreferenceIds == null) return null;

            var preferences = await preferenceRepository.GetRangeByIdsAsync(
                request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await customerRepository.UpdateAsync(customer);

            return customer;
        }

        public async Task<bool> DeleteCustomerAsync(Guid id, [Service] IRepository<Customer> customerRepository)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer == null)
                return false;

            await customerRepository.DeleteAsync(customer);

            return true;
        }
    }
}