﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.ClientSignalR.Models;

namespace Otus.Teaching.PromoCodeFactory.ClientSignalR
{
    internal class Program
    {
        private static async Task Main()
        {
            Console.WriteLine("Test SignalR");
            Console.WriteLine("press any key to continue");
            Console.ReadLine();

            var connection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/customerhub")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();


            var customers = await connection.InvokeAsync<IEnumerable<CustomerResponse>>("GetCustomersAsync");
            Console.WriteLine("\nGetCustomersAsync result:");
            foreach (var c in customers) PrintCustomer(c);


            var customer =
                await connection.InvokeAsync<CustomerResponse>("GetCustomerAsync",
                    "a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
            Console.WriteLine("GetCustomerAsync(id:a6c8c6b1-4349-45b0-ab31-244740aaf0f0) result:");
            PrintCustomer(customer);


            Console.WriteLine("\nCreateCustomerAsync result:");
            var createOrEditCustomerRequest = new CreateOrEditCustomerRequest
            {
                FirstName = "Roland",
                LastName = "Roland Deschain",
                Email = "r.deschain@gamil.com",
                PreferenceIds = new List<Guid> {Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c")}
            };

            var createdCustomer =
                await connection.InvokeAsync<CustomerResponse>("CreateCustomerAsync", createOrEditCustomerRequest);
            PrintCustomer(createdCustomer);


            Console.WriteLine($"\nEditCustomerAsync(id: {createdCustomer.Id}) result:");

            createOrEditCustomerRequest.FirstName += " (edited)";
            createOrEditCustomerRequest.PreferenceIds =
                new List<Guid> {Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")};

            var editedCustomer =
                await connection.InvokeAsync<CustomerResponse>("EditCustomerAsync", createdCustomer.Id,
                    createOrEditCustomerRequest);
            PrintCustomer(editedCustomer);


            Console.WriteLine($"\nDeleteCustomerAsync(id: {createdCustomer.Id}) result:");

            var deleteResult = await connection.InvokeAsync<bool>("DeleteCustomerAsync", createdCustomer.Id);
            Console.WriteLine(deleteResult);
            Console.ReadLine();
        }

        private static void PrintCustomer(CustomerResponse customerResponse)
        {
            Console.WriteLine();
            Console.WriteLine($"    Id: {customerResponse.Id}");
            Console.WriteLine($"    FirstName: {customerResponse.FirstName}");
            Console.WriteLine($"    LastName: {customerResponse.LastName}");
            Console.WriteLine($"    Email: {customerResponse.Email}");
            Console.WriteLine("    Preferences:");
            foreach (var preference in customerResponse.Preferences)
                Console.WriteLine($"        {preference.Id}: {preference.Name}");
            Console.WriteLine();
        }
    }
}