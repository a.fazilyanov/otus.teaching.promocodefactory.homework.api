﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;

namespace Otus.Teaching.PromoCodeFactory.ClientGrpc
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            Console.WriteLine("Test Grpc");
            Console.WriteLine("press any key to continue");
            Console.ReadLine();

            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new GrpcCustomersService.GrpcCustomersServiceClient(channel);


            Console.WriteLine("\nGetCustomersAsync result:");
            var customers = await client.GetCustomersAsync(new Empty());
            foreach (var c in customers.Customers) PrintCustomer(c);


            Console.WriteLine("GetCustomerAsync(id:a6c8c6b1-4349-45b0-ab31-244740aaf0f0) result:");
            var customer = await client.GetCustomerAsync(new StringValue
                {Value = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0"});
            PrintCustomer(customer);


            Console.WriteLine("\nCreateCustomerAsync result:");
            var createOrEditCustomerRequest = new GrpcCreateOrEditCustomerRequest
            {
                FirstName = "Jake",
                LastName = "Chambers",
                Email = "j.chambers@gamil.com"
            };
            createOrEditCustomerRequest.PreferenceIds.AddRange(
                new List<string> {"ef7f299f-92d7-459f-896e-078ed53ef99c"});
            var createdCustomer = await client.CreateCustomerAsync(createOrEditCustomerRequest);
            PrintCustomer(createdCustomer);


            Console.WriteLine($"\nEditCustomerAsync(id: {createdCustomer.Id}) result:");
            createOrEditCustomerRequest.Id = createdCustomer.Id;
            createOrEditCustomerRequest.FirstName += " (edited)";
            createOrEditCustomerRequest.PreferenceIds.Clear();
            createOrEditCustomerRequest.PreferenceIds.AddRange(
                new List<string> {"76324c47-68d2-472d-abb8-33cfa8cc0c84"});
            var editedCustomer = await client.EditCustomerAsync(createOrEditCustomerRequest);
            PrintCustomer(editedCustomer);


            Console.WriteLine($"\nDeleteCustomerAsync(id: {createdCustomer.Id}) result:");

            var deleteResult = await client.DeleteCustomerAsync(new StringValue {Value = createdCustomer.Id});
            Console.WriteLine(deleteResult);
            Console.ReadLine();
        }

        private static void PrintCustomer(GrpcCustomerResponse customerResponse)
        {
            Console.WriteLine();
            Console.WriteLine($"    Id: {customerResponse.Id}");
            Console.WriteLine($"    FirstName: {customerResponse.FirstName}");
            Console.WriteLine($"    LastName: {customerResponse.LastName}");
            Console.WriteLine($"    Email: {customerResponse.Email}");
            Console.WriteLine("    Preferences:");
            foreach (var preference in customerResponse.Preferences)
                Console.WriteLine($"        {preference.Id}: {preference.Name}");
            Console.WriteLine();
        }
    }
}